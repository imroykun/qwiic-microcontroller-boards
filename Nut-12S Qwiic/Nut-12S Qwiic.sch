EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "ESP866 Qwiic"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RF_Module:ESP-WROOM-02 U1
U 1 1 600BE239
P 1950 2000
F 0 "U1" H 2300 2550 50  0000 C CNN
F 1 "ESP-WROOM-02" H 1550 2600 50  0000 C CNN
F 2 "RF_Module:ESP-WROOM-02" H 2550 1450 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/0c-esp-wroom-02_datasheet_en.pdf" H 2000 3500 50  0001 C CNN
	1    1950 2000
	1    0    0    -1  
$EndComp
Text Label 2750 1700 2    50   ~ 0
SDA
Text Label 2750 2200 2    50   ~ 0
SCL
Wire Wire Line
	2750 1700 2450 1700
Wire Wire Line
	2750 2200 2450 2200
$Comp
L power:+3.3V #PWR0101
U 1 1 600BEE57
P 1950 1250
F 0 "#PWR0101" H 1950 1100 50  0001 C CNN
F 1 "+3.3V" H 1965 1423 50  0000 C CNN
F 2 "" H 1950 1250 50  0001 C CNN
F 3 "" H 1950 1250 50  0001 C CNN
	1    1950 1250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 600BF9AC
P 1950 2750
F 0 "#PWR0102" H 1950 2500 50  0001 C CNN
F 1 "GND" H 1955 2577 50  0000 C CNN
F 2 "" H 1950 2750 50  0001 C CNN
F 3 "" H 1950 2750 50  0001 C CNN
	1    1950 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 1400 1950 1250
Wire Wire Line
	1950 2600 1950 2750
Text Label 2750 1600 2    50   ~ 0
IO0
Text Label 2750 1800 2    50   ~ 0
IO4
Text Label 2750 1900 2    50   ~ 0
IO5
Text Label 2750 2000 2    50   ~ 0
IO12
Text Label 2750 2100 2    50   ~ 0
IO13
Text Label 2750 2300 2    50   ~ 0
IO15
Text Label 2750 2400 2    50   ~ 0
IO16
Text Label 1200 2200 0    50   ~ 0
TXD
Wire Wire Line
	2450 1600 2750 1600
Wire Wire Line
	2750 1800 2450 1800
Wire Wire Line
	2450 1900 2750 1900
Wire Wire Line
	2750 2000 2450 2000
Wire Wire Line
	2450 2100 2750 2100
Wire Wire Line
	2750 2300 2450 2300
Wire Wire Line
	2450 2400 2750 2400
Wire Wire Line
	1450 2200 1200 2200
Text Label 1200 2100 0    50   ~ 0
RXD
Text Label 1200 1900 0    50   ~ 0
TOUT
Text Label 1200 1700 0    50   ~ 0
RST
Text Label 1200 1600 0    50   ~ 0
EN
Wire Wire Line
	1200 1600 1450 1600
Wire Wire Line
	1450 1700 1200 1700
Wire Wire Line
	1200 1900 1450 1900
Wire Wire Line
	1450 2100 1200 2100
Text Label 4300 1750 2    50   ~ 0
SDA
Text Label 4300 1650 2    50   ~ 0
SCL
$Comp
L power:+3.3V #PWR0103
U 1 1 600C3501
P 4450 1850
F 0 "#PWR0103" H 4450 1700 50  0001 C CNN
F 1 "+3.3V" H 4465 2023 50  0000 C CNN
F 2 "" H 4450 1850 50  0001 C CNN
F 3 "" H 4450 1850 50  0001 C CNN
	1    4450 1850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 600C48B6
P 4300 1950
F 0 "#PWR0104" H 4300 1700 50  0001 C CNN
F 1 "GND" H 4305 1777 50  0000 C CNN
F 2 "" H 4300 1950 50  0001 C CNN
F 3 "" H 4300 1950 50  0001 C CNN
	1    4300 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 1650 4300 1650
Wire Wire Line
	4100 1750 4300 1750
Wire Wire Line
	4100 1850 4450 1850
Wire Wire Line
	4100 1950 4300 1950
Text Label 5100 1950 0    50   ~ 0
SCL
Text Label 5100 1850 0    50   ~ 0
SDA
$Comp
L power:+3.3V #PWR0105
U 1 1 600C828F
P 4850 1750
F 0 "#PWR0105" H 4850 1600 50  0001 C CNN
F 1 "+3.3V" H 4865 1923 50  0000 C CNN
F 2 "" H 4850 1750 50  0001 C CNN
F 3 "" H 4850 1750 50  0001 C CNN
	1    4850 1750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 600C8852
P 5100 1500
F 0 "#PWR0106" H 5100 1250 50  0001 C CNN
F 1 "GND" H 5105 1327 50  0000 C CNN
F 2 "" H 5100 1500 50  0001 C CNN
F 3 "" H 5100 1500 50  0001 C CNN
	1    5100 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 1950 5100 1950
Wire Wire Line
	5350 1850 5100 1850
Wire Wire Line
	5350 1750 4850 1750
$Comp
L power:GND #PWR0107
U 1 1 600CDBAB
P 1800 5650
F 0 "#PWR0107" H 1800 5400 50  0001 C CNN
F 1 "GND" H 1805 5522 50  0000 R CNN
F 2 "" H 1800 5650 50  0001 C CNN
F 3 "" H 1800 5650 50  0001 C CNN
	1    1800 5650
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Connectors:QWIIC_CONNECTORJS-1MM QWIIC1
U 1 1 600D0B0A
P 4000 1950
F 0 "QWIIC1" H 3958 2560 45  0000 C CNN
F 1 "QWIIC_CONNECTORJS-1MM" H 3958 2476 45  0000 C CNN
F 2 "1X04_1MM_RA" H 4000 2450 20  0001 C CNN
F 3 "" H 4000 1950 50  0001 C CNN
F 4 "CONN-13694" H 3958 2381 60  0000 C CNN "Field4"
	1    4000 1950
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Connectors:QWIIC_CONNECTORJS-1MM QWIIC2
U 1 1 600D1623
P 5450 1650
F 0 "QWIIC2" H 5300 1350 45  0000 C CNN
F 1 "QWIIC_CONNECTORJS-1MM" H 5200 1450 45  0000 C CNN
F 2 "1X04_1MM_RA" H 5450 2150 20  0001 C CNN
F 3 "" H 5450 1650 50  0001 C CNN
F 4 "CONN-13694" H 5300 1550 60  0000 C CNN "Field4"
	1    5450 1650
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J1
U 1 1 600D2E4D
P 2100 6100
F 0 "J1" V 1972 6380 50  0000 L CNN
F 1 "Conn_01x06" V 2063 6380 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Horizontal" H 2100 6100 50  0001 C CNN
F 3 "~" H 2100 6100 50  0001 C CNN
	1    2100 6100
	0    -1   1    0   
$EndComp
Wire Wire Line
	1900 5900 1900 5650
Text Label 2000 5650 3    50   ~ 0
CTS
$Comp
L power:+3.3V #PWR0108
U 1 1 600DFB39
P 2100 5650
F 0 "#PWR0108" H 2100 5500 50  0001 C CNN
F 1 "+3.3V" H 2115 5823 50  0000 C CNN
F 2 "" H 2100 5650 50  0001 C CNN
F 3 "" H 2100 5650 50  0001 C CNN
	1    2100 5650
	1    0    0    -1  
$EndComp
Text Label 2200 5650 3    50   ~ 0
TXD
Text Label 2300 5650 3    50   ~ 0
RXD
Text Label 2400 5650 3    50   ~ 0
DTR
Wire Wire Line
	2000 5900 2000 5650
Wire Wire Line
	2100 5900 2100 5650
Wire Wire Line
	2200 5900 2200 5650
Wire Wire Line
	2300 5900 2300 5650
Wire Wire Line
	2400 5900 2400 5650
Wire Wire Line
	5350 1650 5200 1650
Wire Wire Line
	5200 1650 5200 1500
Wire Wire Line
	5200 1500 5100 1500
$Comp
L Connector:Conn_01x05_Female J2
U 1 1 600E53CA
P 1300 4450
F 0 "J2" H 1200 4900 50  0000 L CNN
F 1 "Conn_01x05_Female" H 900 4800 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x05_P2.54mm_Vertical" H 1300 4450 50  0001 C CNN
F 3 "~" H 1300 4450 50  0001 C CNN
	1    1300 4450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x05_Female J3
U 1 1 600E6716
P 2300 4400
F 0 "J3" H 2250 4000 50  0000 L CNN
F 1 "Conn_01x05_Female" H 1900 4100 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x05_P2.54mm_Vertical" H 2300 4400 50  0001 C CNN
F 3 "~" H 2300 4400 50  0001 C CNN
	1    2300 4400
	-1   0    0    1   
$EndComp
Text Label 850  4250 0    50   ~ 0
EN
Text Label 850  4350 0    50   ~ 0
IO12
Text Label 850  4450 0    50   ~ 0
IO13
Text Label 850  4550 0    50   ~ 0
IO15
Text Label 850  4650 0    50   ~ 0
IO0
Wire Wire Line
	850  4650 1100 4650
Wire Wire Line
	850  4550 1100 4550
Wire Wire Line
	850  4450 1100 4450
Wire Wire Line
	850  4350 1100 4350
Wire Wire Line
	850  4250 1100 4250
Text Label 2750 4600 2    50   ~ 0
IO4
Text Label 2750 4500 2    50   ~ 0
IO5
Text Label 2750 4400 2    50   ~ 0
RST
Text Label 2750 4300 2    50   ~ 0
TOUT
Text Label 2750 4200 2    50   ~ 0
IO16
Wire Wire Line
	2500 4600 2750 4600
Wire Wire Line
	2500 4500 2750 4500
Wire Wire Line
	2500 4400 2750 4400
Wire Wire Line
	2500 4300 2750 4300
Wire Wire Line
	2500 4200 2750 4200
Text Label 7000 1700 0    50   ~ 0
SDA
Text Label 8300 1700 2    50   ~ 0
SCL
$Comp
L Device:R_Small R1
U 1 1 600F13AF
P 7350 1700
F 0 "R1" V 7154 1700 50  0000 C CNN
F 1 "2K" V 7245 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7350 1700 50  0001 C CNN
F 3 "~" H 7350 1700 50  0001 C CNN
	1    7350 1700
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R2
U 1 1 600F1E43
P 7950 1700
F 0 "R2" V 7754 1700 50  0000 C CNN
F 1 "2K" V 7845 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7950 1700 50  0001 C CNN
F 3 "~" H 7950 1700 50  0001 C CNN
	1    7950 1700
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR0109
U 1 1 600F3215
P 7650 1600
F 0 "#PWR0109" H 7650 1450 50  0001 C CNN
F 1 "+3.3V" H 7665 1773 50  0000 C CNN
F 2 "" H 7650 1600 50  0001 C CNN
F 3 "" H 7650 1600 50  0001 C CNN
	1    7650 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 1700 7250 1700
Wire Wire Line
	7450 1700 7650 1700
Wire Wire Line
	7650 1700 7850 1700
Connection ~ 7650 1700
Wire Wire Line
	8050 1700 8300 1700
Wire Wire Line
	7650 1700 7650 1600
Text Label 7800 3200 0    50   ~ 0
IO16
Text Label 8650 3200 2    50   ~ 0
RST
$Comp
L Jumper:SolderJumper_2_Open JP1
U 1 1 600F8818
P 8200 3200
F 0 "JP1" H 8200 3405 50  0000 C CNN
F 1 "DS" H 8200 3314 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 8200 3200 50  0001 C CNN
F 3 "~" H 8200 3200 50  0001 C CNN
	1    8200 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 3200 8050 3200
Wire Wire Line
	8350 3200 8650 3200
Text Label 4400 3500 0    50   ~ 0
CTS
Text Label 4400 4100 0    50   ~ 0
EN
Text Label 5500 3200 2    50   ~ 0
IO0
Text Label 5600 3800 2    50   ~ 0
DTR
$Comp
L Device:R_Small R4
U 1 1 600FD925
P 4800 3500
F 0 "R4" V 4604 3500 50  0000 C CNN
F 1 "10K" V 4695 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4800 3500 50  0001 C CNN
F 3 "~" H 4800 3500 50  0001 C CNN
	1    4800 3500
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R3
U 1 1 600FDDBE
P 5150 3800
F 0 "R3" V 5350 3800 50  0000 C CNN
F 1 "10K" V 5250 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5150 3800 50  0001 C CNN
F 3 "~" H 5150 3800 50  0001 C CNN
	1    5150 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	4400 3500 4600 3500
Wire Wire Line
	4600 3600 4600 3500
Connection ~ 4600 3500
Wire Wire Line
	4600 3500 4700 3500
Wire Wire Line
	4400 4100 4600 4100
Wire Wire Line
	4600 4100 4600 4000
Wire Wire Line
	4900 3500 5050 3500
Wire Wire Line
	4900 3800 5050 3800
Wire Wire Line
	5250 3800 5350 3800
Wire Wire Line
	5350 3700 5350 3800
Connection ~ 5350 3800
Wire Wire Line
	5350 3800 5600 3800
Wire Wire Line
	5350 3300 5350 3200
Wire Wire Line
	5350 3200 5500 3200
$Comp
L dk_Transistors-Bipolar-BJT-Single:BC847B_215 Q1
U 1 1 6011317F
P 4700 3800
F 0 "Q1" H 4887 3747 60  0000 L CNN
F 1 "BC847B_215" H 4887 3853 60  0000 L CNN
F 2 "digikey-footprints:SOT-23-3" H 4900 4000 60  0001 L CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BC847_SER.pdf" H 4900 4100 60  0001 L CNN
F 4 "1727-2921-1-ND" H 4900 4200 60  0001 L CNN "Digi-Key_PN"
F 5 "BC847B,215" H 4900 4300 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 4900 4400 60  0001 L CNN "Category"
F 7 "Transistors - Bipolar (BJT) - Single" H 4900 4500 60  0001 L CNN "Family"
F 8 "https://assets.nexperia.com/documents/data-sheet/BC847_SER.pdf" H 4900 4600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/nexperia-usa-inc/BC847B,215/1727-2921-1-ND/763460" H 4900 4700 60  0001 L CNN "DK_Detail_Page"
F 10 "TRANS NPN 45V 0.1A SOT23" H 4900 4800 60  0001 L CNN "Description"
F 11 "Nexperia USA Inc." H 4900 4900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4900 5000 60  0001 L CNN "Status"
	1    4700 3800
	-1   0    0    1   
$EndComp
$Comp
L dk_Transistors-Bipolar-BJT-Single:BC847B_215 Q2
U 1 1 60115059
P 5250 3500
F 0 "Q2" H 5438 3553 60  0000 L CNN
F 1 "BC847B_215" H 5438 3447 60  0000 L CNN
F 2 "digikey-footprints:SOT-23-3" H 5450 3700 60  0001 L CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BC847_SER.pdf" H 5450 3800 60  0001 L CNN
F 4 "1727-2921-1-ND" H 5450 3900 60  0001 L CNN "Digi-Key_PN"
F 5 "BC847B,215" H 5450 4000 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 5450 4100 60  0001 L CNN "Category"
F 7 "Transistors - Bipolar (BJT) - Single" H 5450 4200 60  0001 L CNN "Family"
F 8 "https://assets.nexperia.com/documents/data-sheet/BC847_SER.pdf" H 5450 4300 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/nexperia-usa-inc/BC847B,215/1727-2921-1-ND/763460" H 5450 4400 60  0001 L CNN "DK_Detail_Page"
F 10 "TRANS NPN 45V 0.1A SOT23" H 5450 4500 60  0001 L CNN "Description"
F 11 "Nexperia USA Inc." H 5450 4600 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5450 4700 60  0001 L CNN "Status"
	1    5250 3500
	1    0    0    -1  
$EndComp
Text Notes 1350 900  0    180  ~ 36
ESP8266
Wire Notes Line
	550  550  550  3100
Wire Notes Line
	550  3100 3150 3100
Wire Notes Line
	3150 3100 3150 550 
Wire Notes Line
	3150 550  550  550 
Text Notes 1050 3600 0    180  ~ 36
General I/O
Wire Notes Line
	550  3200 550  4800
Wire Notes Line
	550  4800 3150 4800
Wire Notes Line
	3150 4800 3150 3200
Wire Notes Line
	3150 3200 550  3200
Text Notes 800  5250 0    180  ~ 36
Programming header
Wire Notes Line
	550  4900 550  6400
Wire Notes Line
	550  6400 3950 6400
Wire Notes Line
	3950 6400 3950 4900
Wire Notes Line
	3950 4900 550  4900
Text Notes 3900 950  0    180  ~ 36
QWIIC sockets
Wire Notes Line
	3250 550  3250 2450
Wire Notes Line
	3250 2450 6600 2450
Wire Notes Line
	6600 2450 6600 550 
Wire Notes Line
	6600 550  3250 550 
Text Notes 4000 2950 0    180  ~ 36
Reset circuit
Wire Notes Line
	3250 2550 3250 4450
Wire Notes Line
	3250 4450 6600 4450
Wire Notes Line
	6600 4450 6600 2550
Wire Notes Line
	6600 2550 3250 2550
Text Notes 7000 1050 0    180  ~ 36
I2C pullup
Wire Notes Line
	6700 550  6700 2100
Wire Notes Line
	6700 2100 8750 2100
Wire Notes Line
	8750 2100 8750 550 
Wire Notes Line
	8750 550  6700 550 
Text Notes 6900 2600 0    180  ~ 36
Deepsleep enable
Wire Notes Line
	6700 2200 6700 3600
Wire Notes Line
	6700 3600 9600 3600
Wire Notes Line
	9600 3600 9600 2200
Wire Notes Line
	9600 2200 6700 2200
Wire Wire Line
	1900 5650 1800 5650
$EndSCHEMATC
