This is a set of simple boards connecting microcontrollers to the I2C using [QWIIC](https://www.sparkfun.com/qwiic)
connectors (including power).

[View on CadLab](https://cadlab.io/projects/qwiic-microcontroller-boards)
